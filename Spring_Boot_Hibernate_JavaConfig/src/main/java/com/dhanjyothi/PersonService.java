package com.dhanjyothi;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
@Service
@Transactional
public class PersonService  {
	
@Autowired
PersonDao dao;

	public void savePerson(Person person) {
		
		dao.savePerson(person);
	}
    
	
	public void updatePerson(Person person) {
		dao.updatePerson(person);
		
	}

	public List<Person> FetchPerson() {
		// TODO Auto-generated method stub
		return dao.FetchPerson();
	}

	public Person getPersonById(int id) {
		// TODO Auto-generated method stub
		return dao.getPersonById(id);
	}

	
	public void deletePerson(Person person) {
		dao.deletePerson(person);
		
	}

}
