package com.dhanjyothi;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
//https://www.javaguides.net/2018/11/spring-data-jpa-query-creation-from-method-names.html
//https://attacomsian.com/blog/spring-data-jpa-query-annotation
@Repository
public class PersonDao {
	@Autowired
	SessionFactory sessionFactory;
	
	public void savePerson(Person p)
	{
		
		sessionFactory.getCurrentSession().save(p);
			}
	
	public void deletePerson(Person p)
	{
	sessionFactory.getCurrentSession().delete(p);
	
	}
	
	public void updatePerson(Person p)
	{
		sessionFactory.getCurrentSession().update(p);
	}
	
	public List<Person> FetchPerson()
	{
		@SuppressWarnings("unchecked")
		List<Person> lp=sessionFactory.getCurrentSession().createQuery("from Person").list();
		System.out.println(lp);
		return lp;
	}
	
	public Person getPersonById(int pid)
	{
	Person lp=(Person)sessionFactory.getCurrentSession().createQuery("from Person where id=:pid").setParameter("pid",pid).uniqueResult();
		return lp;
	}
}
