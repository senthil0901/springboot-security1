package com.dhanjyothi.service;


import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.dhanjyothi.Person;
import com.dhanjyothi.PersonService;


@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestBeanConfig.class })
public class ServiceTest {

    @Autowired
    PersonService personService;
    private Person person;

    @Before
    public void setup() {
        System.out.println(" Before method..");
        person = new Person();
    }

    @Test
    public void testGet() {
        System.out.println("gettin");
        person.setId(1);
        person.setName("revathi");
        person.setPhone("120");
        person.setCity("chennai");
        personService.savePerson(person);
        List<Person> list = personService.FetchPerson();
        System.out.println("Person" + list);
        for (Person personObj : list) {
           
            assertEquals("chennai", personObj.getCity());
        }

    }
}
